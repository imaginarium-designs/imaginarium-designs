<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Australia´s leading site which connects people who are looking to find a partner or just make new friends.">
<title>Australia´s Dating Company</title>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<link rel="stylesheet" type="text/css"  href="../css/bootstrap.css" />
<link rel="stylesheet" href="../css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css"  href="../css/style.css" />
<link rel="stylesheet" type="text/css"  href="../css/responsive.css" />
<script type="text/javascript" src="../js/jquery.1.11.1.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.js"></script>
<script type="text/javascript" src="../js/syntaxhighlighter/shCore.js"></script>
<script src="../js/owl.carousel.js"></script>
<script type="text/javascript" src="../js/main.js"></script>
<script type="text/javascript" src="../js/jquery.isotope.js"></script>
<script>
//$(function(){
//$('#form1').StepizeForm()
//});
function frmSubmit()
{
	$('#form1').submit();
}
$(document).ready(
function(){
$('#form1').StepizeForm();
}
);
</script>
</head>
<body>
<!-- Home Page
    ==========================================-->
<div id="tf-home" class="text-center">
  <div class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"> </div>
      <div class="item"> </div>
      <div class="item"> </div>
      <div class="item"> </div>
      <div class="container">
        <div class="form-area">
          <div class="steps"> <span rel="fstep_1" rel_form="form1" class="step_bar clear_left">1</span> <span rel="fstep_2" rel_form="form1" class="step_bar">2</span> <span rel="fstep_3" rel_form="form1" class="step_bar">3</span> <span rel="fstep_4" rel_form="form1" class="step_bar">4</span> <span rel="fstep_5" rel_form="form1" class="step_bar clear_right">5</span> </div>
        </div>
        <div class="content hidden-sm hidden-xs"> <a href="#tf-about" class="fa fa-angle-down page-scroll"></a> </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Form
    ==========================================--> 

<!-- About Us Section
    ==========================================-->
<div id="tf-about">
  <div class="container">
    <div class="row">
      <div class="col-md-6"> <img src="../img/02.png" class="img-responsive"> </div>
      <div class="col-md-6">
        <div class="about-text">
          <div class="section-title">
            <h4>Welcome to The Australia Dating Company!</h4>
            <hr>
            <div class="clearfix"></div>
          </div>
          <p class="intro">Australia's leading site which connects people who are looking to find a partner or just make new friends. We have thousands of single people           registered across Australia. You can Register for free and search the site anonymously right now - <a href= "http://australiadatingcompany.com/aus/">          AustraliaDatingCompany.com</a> is completely safe, secure and confidential.</p>
          <p class="intro">Join for free at <a href="http://australiadatingcompany.com/aus/">AustraliaDatingCompany.com</a> today.</p>
          <ul class="about-list">
            <li> <span class="fa fa-dot-circle-o"></span>
              <p><strong>Free</strong> - <em>You can try right now at no cost</em> </p>
            </li>
            <li> <span class="fa fa-dot-circle-o"></span>
              <p><strong>Safe</strong> - <em>Completely secure &amp; confidential</em></p>
            </li>
            <li> <span class="fa fa-dot-circle-o"></span>
              <p><strong>Popular</strong> - <em>More active members than any other similar site!</em></p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Why Section
    ==========================================-->
<div id="tf-services" class="text-center">
  <div class="container">
    <div class="section-title center">
      <h2>Why Choose The Australia Dating Company?</strong></h2>
      <div class="line">
        <hr>
      </div>
      <div class="space"></div>
      <div class="row">
        <div class="col-md-3 col-sm-6 service"> <i class="fa fa-thumbs-up"></i>
          <h4><strong>Free to Join</strong></h4>
          <p>Sign up and search for free, try it now.</p>
        </div>
        <div class="col-md-3 col-sm-6 service"> <i class="fa fa-user-plus"></i>
          <h4><strong>The Most Popular</strong></h4>
          <p>2 million + members in our Local &amp; International Network</p>
        </div>
        <div class="col-md-3 col-sm-6 service"> <i class="fa fa-shield"></i>
          <h4><strong>Safe &amp; Secure</strong></h4>
          <p>All profiles are checked, all messages strictly confidential</p>
        </div>
        <div class="col-md-3 col-sm-6 service"> <i class="fa fa-mobile"></i>
          <h4><strong>Mobile, Tablet &amp; PC</strong></h4>
          <p>Our system works for you whenever &amp; where ever you want</p>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Work Examples Section
    ==========================================-->
<div id="tf-testimonials" class="text-center">
  <div class="overlay">
    <div class="container">
      <div class="section-title center">
        <h2>Some Examples Of My Work</h2>
        <div class="line">
          <hr>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div id="testimonial" class="owl-carousel owl-theme">
            <div class="item">
              <h5>"I sent Jackie a message first and we got chatting. We’d both only been on the site for 24 hours but we seemed to have lots in common so we arranged to meet for a coffee. We hit it off straight away and decided to meet again. After our third date we had a kiss and a cuddle and we haven’t been apart since! I’ve since asked Jackie to marry me and she said yes! We both have a few things to sort out first but she is just wonderful."</h5>
              <p>Jon and Jackie</p>
            </div>
            <div class="item">
              <h5>"I winked at John cos he had this lovely humorous grin. He then sent several really nice messages, spiked with humour which made him seem very attractive. After a couple of weeks he dared me to phone him, so I did and we met for lunch the following day. We clicked immediately and shared a kiss. We have been almost inseparable since. Now we are both extremely happy and I have found love again after a very long time on my own."</h5>
              <p>Mary and John</p>
            </div>
            <div class="item">
              <h5>"Things are fantastic with Jennie, I can’t believe we met on a dating site but I’m so glad we did! We weren’t too hasty and spent a couple of weeks sending messages and getting to know each other before we met. Since our first date, we have spent lots of time together and we recently went away for a long weekend. We are so in love! We both lost our long time partners so we understand each other and talking about our pasts has just brought us even closer together."</h5>
              <p>Robert and Jennie</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Footer
    ==========================================-->

<nav id="footer">
  <div class="container">
    <div class="pull-left fnav">
      <p><span class="disclaimer">Testimonials may be from sites in the Australia Dating Company network and not necessarily from the site itself.</span> 
      <p><span class="copyright-text">ALL RIGHTS RESERVED. COPYRIGHT &copy; 2016 Australia Dating Company</span> <span class="not-showin-responsive">|</span> <a href="http://aus.australiadatingcompany.com/help/charter.cfm">Customer Charter</a> | <a href="http://aus.australiadatingcompany.com/help/membership.cfm">Membership</a> | <a href="http://aus.australiadatingcompany.com/help/terms.cfm">Terms of Use</a> | <a href="http://aus.australiadatingcompany.com/help/privacy.cfm">Privacy Policy</a> | <a href="http://aus.australiadatingcompany.com/help/cookies.cfm">Cookie Policy</a></p>
    </div>
    <!--<div class="pull-right fnav">
      <ul class="footer-social">
        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
      </ul>
    </div>-->
  </div>
</nav>
<script>
$('.carousel').carousel({
  interval: 9000,
  pause: false
})
</script> 
<script>
    $('a[href*=#]:not([href=#])').click(function() { 
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
               if (target.length) {
                 $('html,body').animate({
                     scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
</script>

<!-- WLD CAMPAIGN TRACKING -->
<script type="text/javascript">
  var wld_app_id = 51292;
  var wld_app_url = "aus.australiadatingcompany.com";
</script>
<script type="text/javascript">
  var num = Math.floor(Math.random() * 1000000 + 1);
  document.write('<scri'+'pt src="//s.wldcdn.net/media/waldo/tracking/tracker.js?'+num+'"></'+'script>');
  var wld_jsHost = (wld_app_url.match(/^https?:\/\//) ? "" + wld_app_url + "" : document.location.protocol + "//" + wld_app_url);
  document.write(unescape("%3Cscript src='" + wld_jsHost + "/assets/generic/scripts/referral.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
  if (tracking.length){
    document.write(unescape("%3Cscript src='" + wld_jsHost + "/api/tracking/?site=" + wld_app_id + "&" + tracking + "' type='text/javascript'%3E%3C/script%3E"));
  }
</script>
<!-- WLD CAMPAIGN TRACKING -->

</body>
</html>
